
fill = require('spec.aetherflow-fill')

require('core')
require('drawable')
require('.debug')

--[[
TODO: These tests rely heavily on internal workings of the current console.
      The console needs to have it's API locked down and improved such that
      more portable tests may be written.
--]]

describe('debug', function()
	insulate('console', function()
		after_each(function()
			Attunement.Debug.Console:Clear()
		end)

		it('is disabled by default', function()
			assert.is_false(Attunement.Debug.Console.setUp)
		end)

		it('ignores :Write while disabled', function()
			Attunement.Debug.Console:Write('a message')

			assert.are.same({}, Attunement.Debug.Console.messages)
		end)

		it('enables', function()
			local console = Attunement.Debug.Console:Enable()

			assert.is_true(console.setUp)
		end)

		it('overrides print', function()
			local console = Attunement.Debug.Console:Enable()
			local message = 'a message'

			print(message, message)

			local defaultPrefix = console.prefixes.debug
			local expectedMessage = defaultPrefix .. ' ' .. message .. '\t' .. message
			assert.are.same({expectedMessage}, console.messages)
		end)

		it('accepts debug levels in :Write', function()
			local console = Attunement.Debug.Console:Enable()
			local level = 'warning'
			local message = 'a message'

			console:Write(level, message, message)

			local expectedMessage = console.prefixes[level] .. ' ' .. message
			assert.are.same({expectedMessage, expectedMessage}, console.messages)
		end)

		it('is cleared by :Clear', function()
			local console = Attunement.Debug.Console:Enable()

			console:Write('this should not be seen')
			console:Clear()

			assert.are.same({}, console.messages)
		end)

		it('does not exceed max lines', function()
			local console = Attunement.Debug.Console:Enable()
			local maxLines = console.maxLines

			for i = 1, maxLines + 10 do
				console:Write('a message')
			end

			assert.equals(maxLines, #console.messages)
		end)

		it('is used by core for error messages', function()
			local console = Attunement.Debug.Console:Enable()

			Attunement.AddOnLoad(function() error('test error') end)
			UI.OnLoad()

			assert.matches('test error', console.messages[1])
		end)
	end)
end)
