
fill = require('spec.aetherflow-fill')

require('core')

describe('core', function()
	before_each(function()
		Attunement.ClearOnLoad()
		Attunement.ClearOnFrame()
	end)

	it('is defined', function()
		assert.truthy(Attunement.Core)
	end)

	insulate('OnLoad', function()
		-- Check initial state of OnLoad
		it('defines UI.OnLoad', function()
			assert.is_function(UI.OnLoad)
		end)

		it('proxies UI.OnLoad', function()
			local s = spy.new(function() end)
			Attunement.AddOnLoad(s)

			UI.OnLoad()

			assert.spy(s).was.called()
		end)

		it('catches user errors', function()
			local s = spy.new(function() error('test error') end)
			Attunement.AddOnLoad(s)

			UI.OnLoad()

			assert.spy(s).was.called()
			assert.spy(UI.NewLabel).was.called()
			assert.spy(UI.NewLabel).was.called_with('Dialog')
			assert.spy(fill.genericElement.Show).was.called()
			assert.matches('test error', fill.genericElement.Text)
		end)

		it('handles multiple functions', function()
			local functions = {
				spy.new(function() end),
				spy.new(function() end),
				spy.new(function() end)
			}
			for _, func in pairs(functions) do
				Attunement.AddOnLoad(func)
			end

			UI.OnLoad()

			for _, s in ipairs(functions) do
				assert.spy(s).was.called()
			end
		end)

		it('can remove functions', function()
			local s = spy.new(function() end)
			local id = Attunement.AddOnLoad(s)
			Attunement.RemoveOnLoad(id)
			UI.OnLoad()
			assert.spy(s).was.not_called()
		end)
	end)

	insulate('OnFrame', function()
		it('defines UI.OnFrame', function()
			assert.is_function(UI.OnFrame)
		end)

		-- Function mode
		it('proxies UI.OnFrame', function()
			-- Need to proxy to the spy due to the type check
			local s = spy.new(function() end)
			Attunement.AddOnFrame(s)

			local ticks = 12345

			UI.OnFrame(ticks)

			assert.spy(s).was.called()
			assert.spy(s).was.called_with(ticks)
		end)

		it('catches user errors', function()
			local s = spy.new(function() error('test error') end)
			Attunement.AddOnFrame(s)
			local ticks = 12345

			UI.OnFrame(ticks)

			assert.spy(s).was.called()
			assert.spy(s).was.called_with(ticks)
			assert.spy(UI.NewLabel).was.called()
			assert.spy(UI.NewLabel).was.called_with('Dialog')
			assert.spy(fill.genericElement.Show).was.called()
			assert.matches('test error', fill.genericElement.Text)
		end)

		it('handles multiple functions', function()
			local functions = {
				spy.new(function() end),
				spy.new(function() end),
				spy.new(function() end)
			}
			for _, func in pairs(functions) do
				Attunement.AddOnFrame(func)
			end
			local ticks = 12345

			UI.OnFrame(ticks)

			for _, s in ipairs(functions) do
				assert.spy(s).was.called()
				assert.spy(s).was.called_with(ticks)
			end
		end)

		it('can remove functions', function()
			local s = spy.new(function() end)
			local id = Attunement.AddOnFrame(s)
			Attunement.RemoveOnFrame(id)
			UI.OnFrame(12345)
			assert.spy(s).was.not_called()
		end)
	end)

	-- Has to be in a seperate block to test not having DS
	insulate('Interval', function()
		it('errors when data-structures is not included', function()
			local s = spy.new(function() end)

			assert.has.errors(function()
				Attunement.Interval:New(100, s)
			end)
			assert.spy(s).was_not.called()
		end)
	end)

	insulate('Interval', function()
		require('data-structures')

		-- Need to destroy the interval data after each run
		before_each(function()
			Attunement.Interval.setUp = false
		end)

		it('handles a basic interval', function()
			local s = spy.new(function() end)
			Attunement.Interval:New(100, s)

			UI.OnFrame(0)
			assert.spy(s).was_not.called()

			UI.OnFrame(50)
			assert.spy(s).was_not.called()

			UI.OnFrame(100)
			assert.spy(s).was.called(1)

			UI.OnFrame(150)
			assert.spy(s).was.called(1)

			UI.OnFrame(200)
			assert.spy(s).was.called(2)
		end)

		it('repeats to make up for missed intervals', function()
			local s = spy.new(function() end)
			Attunement.Interval:New(10, s)

			UI.OnFrame(0)
			assert.spy(s).was_not.called()

			UI.OnFrame(100)
			assert.spy(s).was.called(10)
		end)

		it('handles multiple intervals', function()
			local s1 = spy.new(function() end)
			local s2 = spy.new(function() end)

			Attunement.Interval:New(30, s1)
			Attunement.Interval:New(100, s2)

			UI.OnFrame(0)
			assert.spy(s1).was_not.called()
			assert.spy(s2).was_not.called()

			UI.OnFrame(50)
			assert.spy(s1).was.called(1)
			assert.spy(s2).was_not.called()

			UI.OnFrame(100)
			assert.spy(s1).was.called(3)
			assert.spy(s2).was.called(1)

			UI.OnFrame(150)
			assert.spy(s1).was.called(5)
			assert.spy(s2).was.called(1)
		end)

		it('can be stopped', function()
			local s = spy.new(function() end)

			local interval = Attunement.Interval:New(30, s)

			UI.OnFrame(0)
			assert.spy(s).was_not.called()

			UI.OnFrame(50)
			assert.spy(s).was.called(1)

			interval:Stop()
			UI.OnFrame(100)
			assert.spy(s).was.called(1)
		end)

		it('can be stopped after a specified number of calls', function()
			local s = spy.new(function() end)

			local interval = Attunement.Interval:New(30, s)
			interval:StopAfter(2)

			UI.OnFrame(0)
			assert.spy(s).was_not.called()

			UI.OnFrame(50)
			assert.spy(s).was.called(1)

			UI.OnFrame(200)
			assert.spy(s).was.called(2)
		end)

		it('can be set up as a once-off easily', function()
			local s = spy.new(function() end)

			Attunement.Interval:Once(30, s)

			UI.OnFrame(0)
			assert.spy(s).was_not.called()

			UI.OnFrame(50)
			assert.spy(s).was.called(1)

			UI.OnFrame(100)
			assert.spy(s).was.called(1)
		end)

		it('passes through the current tick', function()
			local s = spy.new(function(a) end)
			local tick = 534

			Attunement.Interval:Once(30, s)

			UI.OnFrame(0)
			assert.spy(s).was_not.called()

			UI.OnFrame(tick)
			assert.spy(s).was.called(1)
			assert.spy(s).was.called_with(tick)
		end)
	end)
end)
