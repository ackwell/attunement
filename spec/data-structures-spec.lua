
fill = require('spec.aetherflow-fill')

require('data-structures')


describe('data-structures', function()
	describe('FIFOQueue', function()
		it('is empty on creation', function()
			local q = Attunement.DataStructures.FIFOQueue:New()
			assert.equals(true, q:Empty())
		end)
		it('returns nil from Pop() when empty', function()
			local q = Attunement.DataStructures.FIFOQueue:New()
			assert.equals(nil, q:Pop())
		end)
		it('pops 1 item', function()
			local q = Attunement.DataStructures.FIFOQueue:New()
			q:Push(3)
			assert.equals(3, q:Pop())
		end)
		it('pops 2 item', function()
			local q = Attunement.DataStructures.FIFOQueue:New()
			q:Push(3)
			q:Push(7)
			assert.equals(3, q:Pop())
			assert.equals(7, q:Pop())
		end)
		it('pops 3 item', function()
			local q = Attunement.DataStructures.FIFOQueue:New()
			q:Push(3)
			q:Push("Hello")
			q:Push(900)
			assert.equals(3, q:Pop())
			assert.equals("Hello", q:Pop())
			assert.equals(900, q:Pop())
		end)
		it('works in scenario 1', function()
			local q = Attunement.DataStructures.FIFOQueue:New()
			for i=1, 400 do
				q:Push(i)
			end
			for i=1, 399 do
				q:Pop()
			end
			q:Push(1)
			assert.equals(400, q:Pop())
			assert.equals(1, q:Pop())
			assert.equals(nil, q:Pop())
			assert.equals(true, q:Empty())
		end)
		it('supports iteration', function()
			local q = Attunement.DataStructures.FIFOQueue:New()
			local expected = ""
			local actual = ""
			for i=1, 400 do
				q:Push(i)
				expected = expected .. i
			end
			for i in q:Iter() do
				actual = actual .. i
			end
			assert.equals(actual, expected)
		end)
		it('can tell me how much is in it', function()
			local q = Attunement.DataStructures.FIFOQueue:New()
			assert.equals(0, q:Count())
			q:Push(3)
			q:Push("Hello")
			q:Push(900)
			assert.equals(3, q:Count())
			q:Pop()
			q:Pop()
			assert.equals(1, q:Count())
			q:Pop()
			assert.equals(0, q:Count())
		end)
		it('can concat', function()
			local q = Attunement.DataStructures.FIFOQueue:New()
			for i=1,10 do
				q:Push(i)
			end
			assert.equals("1 2 3 4 5 6 7 8 9 10", q:Concat())
			assert.equals("1,2,3,4,5,6,7,8,9,10", q:Concat(","))
			assert.equals("2,3,4,5,6,7,8,9,10", q:Concat(",",2))
			assert.equals("2,3,4", q:Concat(",",2,4))
			for i=11,20 do
				q:Push(i)
			end
			for i=1,10 do
				q:Pop()
			end
			assert.equals("11 12 13 14 15 16 17 18 19 20", q:Concat())
			assert.equals("11,12,13,14,15,16,17,18,19,20", q:Concat(","))
			assert.equals("12,13,14,15,16,17,18,19,20", q:Concat(",",2))
			assert.equals("12,13,14", q:Concat(",",2,4))
		end)
	end)
	describe('Ring', function()
		it('is empty on creation', function()
			local r = Attunement.DataStructures.Ring:New(5)
			assert.equals(true, r:Empty())
			assert.equals(0, r:Count())
		end)
		it('errors on no size', function()
			assert.has.errors(function()
				Attunement.DataStructures.Ring:New()
			end)
		end)
		it('errors when size not a number', function()
			assert.has.errors(function()
				Attunement.DataStructures.Ring:New("H")
			end)
		end)
		it('errors when size is 0', function()
			assert.has.errors(function()
				Attunement.DataStructures.Ring:New(0)
			end)
		end)
		it('errors when size is < 0', function()
			assert.has.errors(function()
				Attunement.DataStructures.Ring:New(-1)
			end)
		end)
		it('errors when size is 0.2', function()
			assert.has.errors(function()
				Attunement.DataStructures.Ring:New(0.2)
			end)
		end)
		it('errors when size is 30.8', function()
			assert.has.errors(function()
				Attunement.DataStructures.Ring:New(30.8)
			end)
		end)
		it('stops growing at size', function()
			local r = Attunement.DataStructures.Ring:New(5)
			r:Push(1)
			assert.equals(1, r:Count())
			r:Push(1)
			assert.equals(2, r:Count())
			r:Push(1)
			assert.equals(3, r:Count())
			r:Push(1)
			assert.equals(4, r:Count())
			r:Push(1)
			assert.equals(5, r:Count())
			r:Push(1)
			assert.equals(5, r:Count())
			r:Push(1)
			assert.equals(5, r:Count())
			r:Push(1)
			assert.equals(5, r:Count())
			r:Push(1)
			assert.equals(5, r:Count())
			r:Push(1)
			assert.equals(5, r:Count())
		end)
		it('has most recent data when Iterated', function()
			local r = Attunement.DataStructures.Ring:New(5)
			local expected = "56789"
			local actual = ""

			for i=1, 9 do
				r:Push(i)
			end

			for item in r:Iter() do
				actual = actual..item
			end
			assert.equals(expected, actual)
		end)
		it('has removed access to Pop() from the base', function()
			local r = Attunement.DataStructures.Ring:New(5)
			assert.equals(nil, r.Pop)
		end)
		it('can concat', function()
			local q = Attunement.DataStructures.Ring:New(10)
			for i=1,10 do
				q:Push(i)
			end
			assert.equals("1 2 3 4 5 6 7 8 9 10", q:Concat())
			assert.equals("1,2,3,4,5,6,7,8,9,10", q:Concat(","))
			assert.equals("2,3,4,5,6,7,8,9,10", q:Concat(",",2))
			assert.equals("2,3,4", q:Concat(",",2,4))
			for i=11,20 do
				q:Push(i)
			end
			assert.equals("11 12 13 14 15 16 17 18 19 20", q:Concat())
			assert.equals("11,12,13,14,15,16,17,18,19,20", q:Concat(","))
			assert.equals("12,13,14,15,16,17,18,19,20", q:Concat(",",2))
			assert.equals("12,13,14", q:Concat(",",2,4))
		end)
	end)

	describe('PriorityQueue', function()
		local pq

		before_each(function()
			pq = Attunement.DataStructures.PriorityQueue:New()
		end)

		it('is empty on creation', function()
			-- Checking pop is nil is the only way to check if it's empty at the moment
			assert.is_true(pq:Empty())
		end)

		it('pops in priority order', function()
			pq:Push{priority = 3, value = '3'}
			pq:Push{priority = 1, value = '1'}
			pq:Push{priority = 2, value = '2'}

			assert.equals('1', pq:Pop().value)
			assert.equals('2', pq:Pop().value)
			assert.equals('3', pq:Pop().value)
		end)

		it('stores any data type', function()
			pq:Push{priority = 1, value = 1}
			pq:Push{priority = 2, value = '2'}
			pq:Push{priority = 3, value = {3}}
			pq:Push{priority = 4, value = true}

			assert.equals(1, pq.Pop().value)
			assert.equals('2', pq.Pop().value)
			assert.same({3}, pq.Pop().value)
			assert.equals(true, pq.Pop().value)
		end)

		it('handles sparse priorities', function()
			pq:Push{priority = 1, value = 1}
			pq:Push{priority = 435623456, value = 3}
			pq:Push{priority = 543623, value = 2}

			assert.equals(1, pq:Pop().value)
			assert.equals(2, pq:Pop().value)
			assert.equals(3, pq:Pop().value)
		end)

		it('returns the popped priority', function()
			pq:Push{priority = 3, value = true}
			pq:Push{priority = 1, value = true}
			pq:Push{priority = 2, value = true}

			for i = 1, 3 do
				assert.equals(i, pq.Pop().priority)
			end
		end)

		it('handles priority collisions gracefully', function()
			pq:Push{priority = 1, value = 1}
			pq:Push{priority = 2, value = 2}
			pq:Push{priority = 2, value = 3}
			pq:Push{priority = 3, value = 4}

			assert.equals(1, pq:Pop().value)

			local popped = pq:Pop().value
			assert.is_true(popped == 2 or popped == 3)
			local other = popped == 2 and 3 or 2
			assert.equals(other, pq:Pop().value)

			assert.equals(4, pq:Pop().value)
		end)

		it('can peek on next value', function()
			pq:Push{priority = 1, value = 1}
			pq:Push{priority = 2, value = 2}

			assert.equals(1, pq:Pop().value)
			assert.equals(2, pq:Peek().value)
			assert.equals(2, pq:Pop().value)
		end)

		it('can be cleared', function()
			pq:Push{priority = 1, value = 1}
			pq:Push{priority = 2, value = 2}

			pq:Clear()

			assert.is_nil(pq:Pop())
			assert.is_true(pq:Empty())
		end)
	end)
end)
