
require('color')
local Color = Attunement.Color

describe('Color', function()
	it('allows () stype construction from the string', function()
		local c = Color:New("#01020304")
		assert.equals(1, c.Alpha)
		assert.equals(2, c.Red)
		assert.equals(3, c.Green)
		assert.equals(4, c.Blue)
		assert.equals("#01020304", c.String)
	end)
	it('allows empty creation with ()', function()
		local c = Color:New()
		assert.equals(255, c.Alpha)
		assert.equals(0, c.Red)
		assert.equals(0, c.Green)
		assert.equals(0, c.Blue)
		assert.equals("#FF000000", c.String)
	end)
	it('Allows table based param construction', function()
		local c = Color:New{Red=200, Green=100, Blue=60, Alpha=22}
		assert.equals(22, c.Alpha)
		assert.equals(200, c.Red)
		assert.equals(100, c.Green)
		assert.equals(60, c.Blue)
		assert.equals("#16C8643C", c.String)
	end)
	it('.Alpha is mutable', function()
		local c = Color:New{Alpha=22}
		assert.equals(22, c.Alpha)
		c.Alpha = 23
		assert.equals(23, c.Alpha)
		assert.equals("#17000000", c.String)
	end)
	it('.Red is mutable', function()
		local c = Color:New{Red=3}
		assert.equals(3, c.Red)
		c.Red = 4
		assert.equals(4, c.Red)
		assert.equals("#FF040000", c.String)
	end)
	it('.Green is mutable', function()
		local c = Color:New{Green=3}
		assert.equals(3, c.Green)
		c.Green = 4
		assert.equals(4, c.Green)
		assert.equals("#FF000400", c.String)
	end)
	it('.Blue is mutable', function()
		local c = Color:New{Blue=3}
		assert.equals(3, c.Blue)
		c.Blue = 4
		assert.equals(4, c.Blue)
		assert.equals("#FF000004", c.String)
	end)
	it('.String is mutable', function()
		local c = Color:New("#01020304")
		assert.equals("#01020304", c.String)
		c.String = "#09080706"
		assert.equals(9, c.Alpha)
		assert.equals(8, c.Red)
		assert.equals(7, c.Green)
		assert.equals(6, c.Blue)
		assert.equals("#09080706", c.String)
	end)
	it('tostring() on a Color is same as .String', function()
		local c = Color:New("#01020304")
		assert.equals(tostring(c), c.String)
		c.String = "#09080706"
		assert.equals(tostring(c), c.String)
	end)
end)
