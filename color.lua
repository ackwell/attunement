--[[
I could put this in drawable it would make some sense
However it does not depend on anything so in the spirit
of being minimal, i'm making it its own file.
--]]

-- Ensure Attunement is defined
if not Attunement then
	Attunement = {}
end

--[[
Helper function that splits the string into the rgba numbers
--]]
local function ToARGB(str)
	if string.len(str) ~= string.len('#01020304') then
		error("Not an ARGB string")
	end
	if string.sub(str, 1, 1) ~= '#' then
		error("Not an ARGB string")
	end
	local a = tonumber(string.sub(str, 2, 3), 16)
	local r = tonumber(string.sub(str, 4, 5), 16)
	local g = tonumber(string.sub(str, 6, 7), 16)
	local b = tonumber(string.sub(str, 8, 9), 16)
	return a, r, g, b
end

--[[
Construct a Color from the '#01020304' string
 or from just rbga.
Allows changing of red, gree, blue, and alpha.
Allow to get the new '#01020304' string out.
--]]
Attunement.Color = {}
function Attunement.Color:Bless(self, protected, params)
	-- Private vars
	local red = 0
	local green = 0
	local blue = 0
	local alpha = 255

	-- Arg checking
	if params == nil then
		-- No args, leave defaults
	elseif type(params) == "table" then
		-- table based args expecting reg, green, blue, and alpha
		-- all are optional
		red   = math.floor(params.Red or red)
		green = math.floor(params.Green or green)
		blue  = math.floor(params.Blue or blue)
		alpha = math.floor(params.Alpha or alpha)
	elseif type(params) == "string" then
		-- parse out from the '#01020304' string
		alpha, red, green, blue = ToARGB(params)
	else
		-- error
		error("Bad args to Color constructor"..tostring(params))
	end

	-- Metatable for self
	local mt = {
		__newindex = function(table, key, value)
			if key == "Red" then
				red = math.floor(value)
			elseif key == "Green" then
				green = math.floor(value)
			elseif key == "Blue" then
				blue = math.floor(value)
			elseif key == "Alpha" then
				alpha = math.floor(value)
			elseif key == "String" then
				alpha, red, green, blue = ToARGB(value)
			end
		end,
		__index = function(table, key)
			if key == "Red" then
				return red
			elseif key == "Green" then
				return green
			elseif key == "Blue" then
				return blue
			elseif key == "Alpha" then
				return alpha
			elseif key == "String" then
				return string.format("#%02X%02X%02X%02X", alpha, red, green, blue)
			end
		end,
		-- Replace __tostring for this table so wh its get printed or marshaled
		-- into a string (like assigning to .Color) it will return the same as .String
		__tostring = function(t)
			return t.String
		end
	}
	setmetatable(self, mt)

	return self, protected
end

-- Default constructor
function Attunement.Color:New(params)
	return Attunement.Color:Bless({}, {}, params)
end
