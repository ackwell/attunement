
Core `attunement/core.lua`
==========================

The core component of Attunement, required when using ***any*** other Attunement components. For best results, make sure core is loaded before other Attunement files in your `.ainf` file. Technically shouldn't make any difference, but just to be on the safe side.

* [Main Functions](#main-functions)
* [Interval & Timers](#intervals-and-timers)

Main Functions
--------------

Attunement provides wrappers around Aetherflow's `UI` function hooks, that allows the library to automatically handle the dirty work for you. When using Attunement, **make sure** you use these wrapper functions. Failure to do so may result in some pretty funky undefined behaviour.

All of these wrappers execute with error handling. Should something go wrong, a nasty red error message will be displayed to the user, or alternatively an error message will be printed to the [attunement console](docs/debug.md#console) if it is enabled.

The wrappers are designed to allow multiple handlers to be registered for each event, however due to the nature of Lua's implementation of tables, the order of execution for these functions ***is not guaranteed*** in any way. Keep that in mind while writing your code.

### Attunement.AddOnLoad(func)

Registers a new function to handle the OnLoad event. Any number may be added, any registered at the time the addon is initialised by Aetherflow will be called. An ID will be returned, that may be used to remove the handler at a later time.

`OnLoad` is best used to set up any primary drawables and initial values required by your addon.

### Attunement.AddOnFrame(func)

As with `Attunement.AddOnLoad`, multiple functions may be registered with this function - all will be called on each execution of `Ui.OnFrame`. The callback `func` will be passed the current `ticks` value. An ID will be returned, to allow the removal of the handler at a later time.

### Attunement.RemoveOnLoad(id), Attunement.RemoveOnFrame(id)

These functions can be used to remove previously created handlers. The parameter `id` should be the ID returned from `AddOnLoad`/`AddOnFrame`.

Intervals and timers
--------------------

Also included in the `core` module is the `Interval` system, which allows you to specify a function that will be executed at a set interval (funny that).

### Attunement.Interval:New(interval, func)

Registers a new interval to execute `func` every `interval` milliseconds. The function is guaranteed to be run as close to the interval as possible, even if that would potentially involve running multiple times per frame. As such, setting `interval = 1` is... inadvisable.

The callback function `func` will be passed the current frame `tick` as its first parameter.

Returns an `Interval` object.

### Attunement.Interval:Once(interval, func)

Convenience function, creates an interval to run `func` once after `interval` milliseconds. Interval will not repeat. Current ticks will be passed to the callback. Equivalent to:

```lua
local interval = Attunement.Interval:New(inteval, func)
interval:StopAfter(1)
```

### Interval:Stop()

Stops the interval from running. The function will *not* be run again after calling this. Equivalent to `Interval:StopAfter(0)`.

### Interval:StopAfter(num)

Stops the interval after it has been run `num` more times. Eg, `:StopAfter(1)` will result in the function being run one more time before being stopped.
