DataStructures `attunement/data-structures.lua`
==================================

Generic data structurs such as queues.

Objects:
* FIFOQueue
* Ring

FIFOQueue
-------

Impliments a FIFO (First In First Out) Queue sctructure.
The depth has no limit and it allows storage of mixed types.

### Function Reference

**Attunement.DataStructures.FIFOQueue:New()**

Creates a new FIFOQueue

**FIFOQueue:Push( item )**

Adds `item` to the back of the queue.

**FIFOQueue:Pop()**

Returns the oldest thing in the queue.

**FIFOQueue:Empty()**

Returns `true` if the queue is empty otherwise `false`.

**FIFOQueue:Count()**

Returns number of items in the queue.

**FIFOQueue:Iter()**

Returns iterator that allows looping through the queue without removing items.

```lua
local q = Attunement.DataStructures.FIFOQueue:New()
for i=1, 400 do
	q:Push(i)
end
for i in q:Iter() do
	print(i)
end
```

**FIFOQueue:Concat([sep [, first [, last]]])**

Will join the items in the queue starting with `first` going till and including `last` placing `sep` between each item.

All 3 args are optional defaults are:
* sep = `" "`
* first = 1
* last = FIFOQueue:Count()

Ring
-------

Impliments a Ring sctructure.

The depth has fixed size, as items are added and the Ring grows past the size
the oldest items are throwen away to keep the Ring a fixed size.

Iteration of the Ring with `:Iter()` will provide items from oldest to newest.

### Function Reference

**Attunement.DataStructures.Ring:New(size)**

Creates a new Ring of `size` items max.

Size must be a number >= 1

**Ring:Push( item )**

Adds `item` to the Ring.

**Ring:Empty()**

Returns `true` if the Ring is empty otherwise `false`.

**Ring:Count()**

Returns number of items in the Ring.

**Ring:Iter()**

Returns iterator that allows looping through the Ring.

```lua
local r = Attunement.DataStructures.Ring:New(30)
for i=1, 400 do
	r:Push(i)
end
for i in r:Iter() do
	print(i)
end
```

**Ring:Concat([sep [, first [, last]]])**

Will join the items in the ring starting with `first` going till and including `last` placing `sep` between each item.

All 3 args are optional defaults are:
* sep = `" "`
* first = 1
* last = Ring:Count()

PriorityQueue
-------------

Implementation of a priority queue. Tables added to the queue will be returned in order of lowest to highest priority value.

### Function Reference

**Attunement.DataStructures.PriorityQueue:New()**

Creates a new priority queue for use.

**PQ:Push(table)**

Pushes the provided table onto the queue. If no `priority` key is set, it will be set to `0` (meaning it will likely be returned by the next `:Pop()`) All other keys may be arbitrary.

**PQ:Pop()**

Pops the table with the lowest priority off the queue and returns it.

**PQ:Peek()**

Returns the next table that will be popped, without removing it from the queue.

