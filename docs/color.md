
Color `attunement/color.lua`
============================

Depends on:
* None

This class allows for the transform from `#01020304` color string to a table like structure with `.Alpha`, `.Red`, `.Green`, and `.Blue` attributes that are numbers then back to the `#01020304` string.

In addition, including this module will enable the `Drawable` module to return color attributes as mutable `Color` objects.

### Basic Usage

```lua
-- Can make from the string
local c1 = Color:New("#01020304")
-- Can just make one, default `#FF000000`
local c2 = Color:New()
-- Can make based on table, all 4 are optional
local c3 = Color:New{Red=200, Green=100, Blue=60, Alpha=22}
-- Can change (or read) the values
c3.Alpha = 9
c3.Red = 8
C3.Green = 7
c3.Blue = 6
-- Get the string version of the current color
local s = c3.String
-- Can also change them all by setting String
c3.String = '#02030405'
-- Will implicitly use .String when Coerced to a string
print(c3) -- Prints '#02030405'
```

### Attribute Reference

**.Alpha, .Red, .Green, .Blue**

Allows read/write access to the `Colors` components. Values will be returned as numbers.

**.String**

On read, generates the string version of the current color.

On write, changes all the components of the current color.
