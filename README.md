![icon](icon.png) Attunement
============================

[![build status](https://gitlab.com/ci/projects/105169/status.png?ref=master)](https://gitlab.com/ci/projects/105169?ref=master)

A utility library for Aetherflow.

Current components:

* `Drawable`
* `Debug`
* `Data Structures`

(Wow a whole two components. Keep an eye out for more as we work on this.)

To use, just include `core.lua` and the component files you'd like to use in your addon in your `.ainf` file's `FileList` section.

While work has been put into reducing possible issues caused by load order, for best results ensure that all attunement components are loaded before your own code.

Some components rely on other components - this will be outlined in the documentation for that component.

Component Documentation
-----------------------

* [Core](docs/core.md)
* [Drawable](docs/drawable.md)
* [Debug](docs/debug.md)
* [Data Structures](docs/data-structures.md)
* [Color](docs/color.md)