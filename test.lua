
Attunement.AddOnLoad(function()
	-- Enable the console
	local console = Attunement.Debug.Console:Enable()
	print([[                   _   _   _                                         _                 
  _____ _____     / \ | |_| |_ _   _ _ __   ___ _ __ ___   ___ _ __ | |_   _____ _____ 
 |_____|_____|   / _ \| __| __| | | | '_ \ / _ \ '_ ` _ \ / _ \ '_ \| __| |_____|_____|
 |_____|_____|  / ___ \ |_| |_| |_| | | | |  __/ | | | | |  __/ | | | |_  |_____|_____|
               /_/   \_\__|\__|\__,_|_| |_|\___|_| |_| |_|\___|_| |_|\__|              ]])
	console:Write('This was a triumph!', 'I\'m making a note here:')
	-- Testing the new color class well i'm at it
	local bgColor =  Attunement.Color:New{Red=68, Green=68, Blue=68}

	local text = Attunement.Label:New{
		X = 800,
		Width = 1000,
		Height = 500,
		Text = 'Hello world',
		Hidden = false,

		BorderWidth = 1,
		BorderColor = '#FF000000',
		DrawBackground = true,
		BackgroundColor = bgColor.String,

		Draggable = true,

		FontFamily = 'Sego UI',
		TextAlignment = Attunement.Label.ALIGN_CENTER,
		Layer = 'Dialog'
	}
	text.OnMouseLeftDown = function()
		text.Text = 'click'
	end

	local reset = false
	Attunement.Interval:New(5000, function()
		if reset then return end
		reset = true
		console:Write('deleting')
		text:Delete()
	end)

	local show = false
	Attunement.Interval:New(7500, function()
		if show then return end
		show = true
		console:Write('new')
		local new = Attunement.Label:New{
			Layer = 'Dialog',
			Text = 'New',
			X = 1500,
			Y = 0,
			Width=64
		}
		new:Show()
	end)
end)
